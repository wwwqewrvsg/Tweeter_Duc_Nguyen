//
//  ViewController.swift
//  Tweeter
//
//  Created by DucNguyen on 5/29/18.
//  Copyright © 2018 DucNguyen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	@IBOutlet weak var inputMessageView: UITextView!
	@IBOutlet weak var resultMessageView: UITextView!
	@IBOutlet weak var btnSplit: UIButton!
	
	fileprivate var tap: UITapGestureRecognizer? = nil
	fileprivate let lengthToSplitMessage = 50
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		setupDismissKeyboardGesture()
	}
	
	func setupDismissKeyboardGesture() {
		tap = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
		view.addGestureRecognizer(tap!)
	}
	
	@objc func dismissKeyboard() {
		view.endEditing(true)
	}
	
	func removeDismissKeyboardGesture() {
		if (tap != nil) {
			view.removeGestureRecognizer(tap!)
		}
	}

	@IBAction func splitButtonClick(_ sender: Any) {
		let messageString = inputMessageView.text!
		if (messageString.count > lengthToSplitMessage) {
			if (!messageString.containsWhitespace) {
				let alert = UIAlertController(title: "ERROR", message: "the message must be not contains a span of non-whitespace characters longer than 50 characters!", preferredStyle: UIAlertControllerStyle.alert)
				alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
				self.present(alert, animated: true, completion: nil)
				return
			}
			let splitResult = splitMessage(message: messageString)
			resultMessageView.text = splitResult.joined(separator: "\n")
		} else {
			resultMessageView.text = inputMessageView.text
		}
	}
	
	func splitMessage(message: String) -> [String] {
		let splitArray = message.components(separatedBy: .whitespaces)
		if (splitArray.count == 0) {
			return []
		}
		
		var listResult:[String] = []
		var partIndicator = 1
		var tmpMessage = String(partIndicator) + "/2"
		
		for (index, subString) in splitArray.enumerated() {
			if (tmpMessage.count + subString.count > lengthToSplitMessage) {
				listResult.append(tmpMessage)
				partIndicator += 1
				tmpMessage = String(partIndicator) + "/2"
			}
			
			tmpMessage += " " + subString
			if (index == splitArray.count - 1) {
				listResult.append(tmpMessage)
			}
		}
		
		return listResult
	}
}

