//
//  String+Utils.swift
//  Tweeter
//
//  Created by DucNguyen on 5/29/18.
//  Copyright © 2018 DucNguyen. All rights reserved.
//

extension String {
	var containsWhitespace : Bool {
		return (self.rangeOfCharacter(from: .whitespaces) != nil)
	}
}
