//
//  TweeterTests.swift
//  TweeterTests
//
//  Created by DucNguyen on 5/29/18.
//  Copyright © 2018 DucNguyen. All rights reserved.
//

import XCTest
@testable import Tweeter

class TweeterTests: XCTestCase {
    var controllerUnderTest: ViewController!
	
    override func setUp() {
		controllerUnderTest = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as! ViewController
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
		controllerUnderTest = nil
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
	
	func testSplitMessageValid() {
		let splitArray = controllerUnderTest.splitMessage(message: "I can't believe Tweeter now supports chunking my messages, so I don't have to do it myself.")
		let arrayToCompare = ["1/2 I can't believe Tweeter now supports chunking",
							  "2/2 my messages, so I don't have to do it myself."]
		XCTAssertTrue(splitArray == arrayToCompare, "This function not work")
	}
	
	func testSplitMessageInvalid() {
		let splitArray = controllerUnderTest.splitMessage(message: "I_can't_believe_Tweeter_now_supports_chunking_my_messages,_so_I_don't_have_to_do_it_myself. asds sd asa a")
		let invalidStringArray = splitArray.filter({ (subString) -> Bool in
			return subString.count > 50
		})
		XCTAssertTrue(invalidStringArray.count == 0, "This function not work because the message contains a span of non-whitespace characters longer than 50 characters")
	}
}
